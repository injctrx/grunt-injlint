/*
 * grunt-injlint
 * http://bitbucket.org/injctrx/grunt-injlint
 *
 * Copyright (c) 2019 Injctrx
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    jshint: {
      all: [
        'Gruntfile.js',
        'tasks/*.js'
      ],
      options: {
        jshintrc: '.jshintrc',
        reporterOutput: ''
      }
    },

    // Configuration to be run (and then tested).
    injlint: {
      default: {
        options: {
          dictionary: '.injlintdic',
          nodoublebr: true
        },
        files: {
          src: [
            './test/*.html'
          ]
        }
      }
    }

  });

  // Actually load this plugin's task(s).
  grunt.loadTasks('tasks');

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-jshint');

  // Whenever the "test" task is run, run this plugin's task(s)
  grunt.registerTask('test', ['injlint']);

  // By default, lint and run all tests.
  grunt.registerTask('default', ['jshint', 'test']);

};

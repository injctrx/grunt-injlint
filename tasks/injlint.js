/*
 * grunt-injlint
 * http://bitbucket.org/injctrx/grunt-injlint
 *
 * Copyright (c) 2019 Injctrx
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function (grunt) {

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks

  grunt.registerMultiTask('injlint', 'A multi-task to validate your HTML files', function () {
    var options = this.options({
      dictionary: '.injlintdic',
      nodoublebr: true
    });

    var spell = require('spell-checker-js');
    spell.load(options.dictionary);

    this.files.forEach(function (f) {

      var src = f.src.filter(function (filepath) {
        if (!grunt.file.exists(filepath)) {
          grunt.log.warn('Source file "' + filepath + '" not found.');
          return false;
        } else {
          return true;
        }
      }).map(function (filepath) {

        var fileContent = grunt.file.read(filepath);
        var output = [];
        if (fileContent.match(/class="(.*?)"/g)) {
          fileContent.match(/class="(.*?)"/g).forEach(function (classnames) {
            classnames.replace("class=\"", "").replace("\"", "").split(" ").forEach(function (classname) {
              output.push(classname);
            });
          });
        }

        var outputUnique = output.filter(function (item, index) {
          return output.indexOf(item) >= index;
        }).sort();

        grunt.log.writeln("======================================\n# Classnames spelling mistakes from " + filepath + "\n======================================\n");

        if (!outputUnique.length) {
          grunt.log.writeln("Nothing\n");
        }

        outputUnique.forEach(function (classname) {
          var spellResult = spell.check(classname);
          if (spellResult.length) {
            grunt.log.warn(spellResult[0]);
          }
        });        

        // Check if there are double <br> tags
        if (options.nodoublebr) {
          var fileContentTmp = fileContent.replace(/\s/g, "").replace(/<br\/>/g, "<br>");
          if (fileContentTmp.match(/<br><br>/g)) {
            var errorCount = fileContentTmp.match(/<br><br>/g).length;
            grunt.log.warn("Do not use double <br> tags (Found " + errorCount + " errors)");
          }
        }

        grunt.log.writeln("\n");

      });

    });
  });

};
